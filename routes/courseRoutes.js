const express = require('express')
const router = express.Router()
const CourseController = require('../controllers/CourseController')
const auth = require('../auth')

// Create single course
router.post("/create", auth.verify, (request, response) => {
	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	CourseController.addCourse(data).then((result) => { //Promise
		response.send(result)
	})
})

// Get all courses
router.get("/", (request, response) => {
	CourseController.getAllCourses().then((result) => {
		response.send(result)
	})
})

// Get all ACTIVE courses
router.get("/active", (request, response) => {
	CourseController.getAllActive().then((result) => {
		response.send(result)
	})
})

// Get single course
router.get("/:courseID", (request, response) => {
	CourseController.getCourse(request.params.courseID).then((result) => {
		response.send(result)
	})
})

// Update single course
router.patch("/:courseID/update", auth.verify, (request, response) => {
	CourseController.updateCourse(request.params.courseID, request.body).then((result) => {
		response.send(result)
	})
})

// MINI ACTIVITY
router.patch("/:courseID/archive", auth.verify, (request, response) => {
	CourseController.archiveCourse(request.params.courseID).then((result) => {
		response.send(result)
	})
})

module.exports = router